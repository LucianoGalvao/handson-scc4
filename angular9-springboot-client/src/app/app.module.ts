import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { AppComponent } from './app.component';
import { CreateRemetenteComponent } from './create-remetente/create-remetente.component';
import { RemetenteDetailsComponent } from './remetente-details/remetente-details.component';
import { RemetenteListComponent } from './remetente-list/remetente-list.component';
import { UpdateRemetenteComponent } from './update-remetente/update-remetente.component';
import { AppRoutingModule } from './app.routing.module';
import { HttpClientModule } from '@angular/common/http';
import { NgxMaskModule } from 'ngx-mask'
import { DestinatariosComponent } from './destinatarios/destinatarios.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateRemetenteComponent,
    RemetenteDetailsComponent,
    RemetenteListComponent,
    UpdateRemetenteComponent,
    DestinatariosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxMaskModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
