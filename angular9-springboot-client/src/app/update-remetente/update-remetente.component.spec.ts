import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateRemetenteComponent } from './update-remetente.component';

describe('UpdateRemetenteComponent', () => {
  let component: UpdateRemetenteComponent;
  let fixture: ComponentFixture<UpdateRemetenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateRemetenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateRemetenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
