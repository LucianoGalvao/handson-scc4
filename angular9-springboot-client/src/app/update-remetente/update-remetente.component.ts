import { Component, OnInit } from '@angular/core';
import { Remetente } from '../remetente';
import { ActivatedRoute, Router } from '@angular/router';
import { RemetenteService } from '../remetente.service';

@Component({
  selector: 'app-update-remetente',
  templateUrl: './update-remetente.component.html',
  styleUrls: ['./update-remetente.component.css']
})
export class UpdateRemetenteComponent implements OnInit {

  id: number;
  remetente: Remetente;
  submitted = false;

  constructor(private route: ActivatedRoute, private router: Router,
    private remetenteService: RemetenteService) { }

  ngOnInit() {
    this.remetente = new Remetente();

    this.id = this.route.snapshot.params['id'];

    this.remetenteService.getRemetentes(this.id)
      .subscribe(data => {
        console.log(data)
        this.remetente = data;
      },error => console.log(error))
  }

  updateRemetente(){
    this.remetenteService.updateRemetente(this.id, this.remetente)
      .subscribe(data => console.log(data), error => console.log(error));
    this.remetente = new Remetente();
    this.gotoList();
  }

  onSubmit(){
    this.updateRemetente();
    this.submitted = true;
  }

  gotoList() {
    this.router.navigate(['/remetentes']);
  }

}
