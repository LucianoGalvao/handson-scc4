import { TestBed } from '@angular/core/testing';

import { RemetenteService } from './remetente.service';

describe('RemetenteService', () => {
  let service: RemetenteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RemetenteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
