import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RemetenteService {

  private readonly baseUrl = 'http://localhost:8080/api/remetentes';
  private readonly baseUrlDestinatarios = 'http://localhost:8080/api/destinatarios'

  constructor(private http: HttpClient) { }
  
  uploadCsv(file: any) {
    return this.http.post<any>(this.baseUrlDestinatarios + '/upload-csv-destinatarios', file);
  }

  listarUnico(id: any) {
    return this.http.get<Object>(this.baseUrl + '/remetente/'+ id);
  }

  listar() {
    return this.http.get<Array<Object[]>>(this.baseUrl + '/listarRemetentes');
  }
  
  enviar(formulario: any) {
    return this.http.post(this.baseUrlDestinatarios + '/recebeMensagem', formulario);
  }
  getRemetentes(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`)
  }

  createRemetente(remetente: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, remetente);
  }

  listarDestinatarios() {
    return this.http.get<Array<Object[]>>(this.baseUrlDestinatarios + '/listarDestinatarios');
  }

  updateRemetente(id: number, value: any): Observable<Object> {
    return this.http.post(`${this.baseUrl}/${id}`, value);
  }
  
  deleteRemetente(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text'});
  }
  getRemetentesList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`)
  }
  
}
