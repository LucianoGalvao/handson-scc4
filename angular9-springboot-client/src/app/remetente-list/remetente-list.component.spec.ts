import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemetenteListComponent } from './remetente-list.component';

describe('RemetenteListComponent', () => {
  let component: RemetenteListComponent;
  let fixture: ComponentFixture<RemetenteListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemetenteListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemetenteListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
