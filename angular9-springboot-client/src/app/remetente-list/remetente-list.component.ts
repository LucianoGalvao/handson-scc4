import { Component, OnInit } from '@angular/core';
import { RemetenteService } from '../remetente.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Remetente } from '../remetente';

@Component({
  selector: 'app-remetente-list',
  templateUrl: './remetente-list.component.html',
  styleUrls: ['./remetente-list.component.css']
})
export class RemetenteListComponent implements OnInit {
  remetentes: Observable<Remetente[]>

  constructor(private remetenteService: RemetenteService,
      private router: Router) {}

  ngOnInit() {
    this.reloadData();
  }
  reloadData() {
    this.remetentes = this.remetenteService.getRemetentesList();
  }

  deleteRemetente(id: number) {
    this.remetenteService.deleteRemetente(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  remetenteDetails(id: number){
    this.router.navigate(['details',id])
  }
}
