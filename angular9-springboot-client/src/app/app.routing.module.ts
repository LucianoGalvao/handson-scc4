import { Routes, RouterModule } from '@angular/router';
import { RemetenteListComponent } from './remetente-list/remetente-list.component';
import { CreateRemetenteComponent } from './create-remetente/create-remetente.component';
import { UpdateRemetenteComponent } from './update-remetente/update-remetente.component';
import { RemetenteDetailsComponent } from './remetente-details/remetente-details.component';
import { NgModule } from '@angular/core';
import { DestinatariosComponent } from './destinatarios/destinatarios.component';
import { MensagemComponent } from './destinatarios/mensagem/mensagem.component';

const routes: Routes = [
    { path: '', redirectTo: 'remetentes', pathMatch: 'full'},
    { path: 'remetentes', component: RemetenteListComponent },
    { path: 'adicionar', component: CreateRemetenteComponent },
    { path: 'updateRemetente/:id', component: UpdateRemetenteComponent },
    { path: 'details/:id', component: RemetenteDetailsComponent },
    { path: 'destinatarios', component: DestinatariosComponent },
    { path: 'destinatarios/mensagem', component: MensagemComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }