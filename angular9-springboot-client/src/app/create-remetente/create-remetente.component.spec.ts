import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRemetenteComponent } from './create-remetente.component';

describe('CreateRemetenteComponent', () => {
  let component: CreateRemetenteComponent;
  let fixture: ComponentFixture<CreateRemetenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateRemetenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRemetenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
