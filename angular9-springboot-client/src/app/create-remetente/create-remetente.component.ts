import { Component, OnInit } from '@angular/core';
import { Remetente } from '../remetente';
import { RemetenteService } from '../remetente.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-create-remetente',
  templateUrl: './create-remetente.component.html',
  styleUrls: ['./create-remetente.component.css']
})

export class CreateRemetenteComponent implements OnInit {

  remetente: Remetente = new Remetente();
  submitted = false;

  constructor(private remetenteService: RemetenteService, 
    private router: Router) { }

  ngOnInit() {
  }

  newRemetente(): void {
    this.submitted = false;
    this.remetente = new Remetente();
  }

  save() {
    this.remetenteService.createRemetente(this.remetente)
      .subscribe(data => console.log(data), error=> console.log(error));
    this.remetente = new Remetente;
    this.gotoList();
  }

  onSubmit(){
    this.submitted = true;
    this.save();
  }

  gotoList(){
    this.router.navigate(['/remetentes']);
  }
  
}
