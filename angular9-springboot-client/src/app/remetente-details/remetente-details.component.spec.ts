import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemetenteDetailsComponent } from './remetente-details.component';

describe('RemetenteDetailsComponent', () => {
  let component: RemetenteDetailsComponent;
  let fixture: ComponentFixture<RemetenteDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemetenteDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemetenteDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
