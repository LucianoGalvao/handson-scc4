import { Component, OnInit } from '@angular/core';
import { Remetente } from '../remetente';
import { ActivatedRoute, Router } from '@angular/router';
import { RemetenteService } from '../remetente.service';

@Component({
  selector: 'app-remetente-details',
  templateUrl: './remetente-details.component.html',
  styleUrls: ['./remetente-details.component.css']
})
export class RemetenteDetailsComponent implements OnInit {

  id: number;
  remetente: Remetente;

  constructor(private route: ActivatedRoute, private router: Router,
    private remetenteService: RemetenteService) { }

  ngOnInit() {
    this.remetente = new Remetente();

    this.id = this.route.snapshot.params['id'];

    this.remetenteService.getRemetentes(this.id)
      .subscribe(data => {
        console.log(data)
        this.remetente = data;
      }, error => console.log(error));
  }

  list(){
    this.router.navigate(['/remetentes'])
  }
}
