
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { RemetenteService } from '../remetente.service';

@Component({
  selector: 'app-destinatarios',
  templateUrl: './destinatarios.component.html',
  styleUrls: ['./destinatarios.component.css']
})
export class DestinatariosComponent implements OnInit {

  baseUrlDestinatarios = 'http://localhost:4200/destinatarios';

  form: FormGroup;

  listaDeDestinatarios: Object[] = [];

  constructor(
    private http: HttpClient, 
    private service: RemetenteService) { }

  fileToUpload: File = null;

  ngOnInit() {

    this.service.listarDestinatarios().subscribe(listaDeDestinatarios => {
      this.listaDeDestinatarios = listaDeDestinatarios;
    });

  }

  uploadFile(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({
      avatar: file
    });
    this.form.get('avatar').updateValueAndValidity()
  }

  submitForm() {
    var formData: any = new FormData();
    formData.append("avatar", this.form.get('avatar').value);
    this.http.post(this.baseUrlDestinatarios + '/upload-csv-destinatarios', formData).subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    )
  }
  

}
