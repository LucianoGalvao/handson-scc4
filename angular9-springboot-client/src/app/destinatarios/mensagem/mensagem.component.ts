import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, NgForm, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { elementEventFullName } from '@angular/compiler/src/view_compiler/view_compiler';
import { map } from "rxjs/operators";
import { RemetenteService } from 'src/app/remetente.service';

@Component({
  selector: 'app-mensagem',
  templateUrl: './mensagem.component.html',
  styleUrls: ['./mensagem.component.css']
})
export class MensagemComponent implements OnInit {

  formulario: FormGroup;

  remetente: any;
  remetenteUnico: Object = [];
  actRoute: any;
  url: 'http://localhost:4200/destinatarios/recebeMensagem';
  contactForm: FormGroup;
  route: any;

  
  constructor(private service: RemetenteService) {
    
  }

  listaDeRementetes: Object[] = [];
  listaDeDestinatarios: Object[] = [];

  id: any;
  ngOnInit() {

    this.remetente = {};
    this.service.listar().subscribe(listarRemetentes => {
      this.listaDeRementetes = listarRemetentes;
    });

    this.service.listarDestinatarios().subscribe(listarDestinatarios => {
      this.listaDeDestinatarios = listarDestinatarios;
    });
    
  }

  listarUnico() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          //console.log(this.id);
        }
      );
    
    this.service.listarUnico(this.id).subscribe(remetente => {
      //console.log(remetente);
      this.remetenteUnico = remetente;
    });
  }

  enviar(formulario: FormGroup) {
    this.service.enviar(this.formulario.value).pipe(map(res => res)).subscribe(resposta => {
      this.formulario.reset();
    });
  }

}
