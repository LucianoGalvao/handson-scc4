export class Remetente {
    id: number;
    nomeCompleto: string;
    telefone: string;
    cpf: string;
    endereco: string;
    email: string;
}