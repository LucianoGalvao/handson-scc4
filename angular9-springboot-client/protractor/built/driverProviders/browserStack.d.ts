/// <reference types="q" />
import * as q from 'protractor/built/driverProviders/node_modules/protractor/node_modules/@types/q';
import { Config } from 'protractor/built/driverProviders/node_modules/protractor/built/config';
import { DriverProvider } from 'protractor/built/driverProviders/node_modules/protractor/built/driverProviders/driverProvider';
export declare class BrowserStack extends DriverProvider {
    browserstackClient: any;
    constructor(config: Config);
    /**
     * Hook to update the BrowserStack job status.
     * @public
     * @param {Object} update
     * @return {q.promise} A promise that will resolve when the update is complete.
     */
    updateJob(update: any): q.Promise<any>;
    /**
     * Configure and launch (if applicable) the object's environment.
     * @return {q.promise} A promise which will resolve when the environment is
     *     ready to test.
     */
    protected setupDriverEnv(): q.Promise<any>;
}
