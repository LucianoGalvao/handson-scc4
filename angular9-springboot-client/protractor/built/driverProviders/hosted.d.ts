/// <reference types="q" />
import * as q from 'protractor/built/driverProviders/node_modules/protractor/node_modules/@types/q';
import { Config } from 'protractor/built/driverProviders/node_modules/protractor/built/config';
import { DriverProvider } from 'protractor/built/driverProviders/node_modules/protractor/built/driverProviders/driverProvider';
export declare class Hosted extends DriverProvider {
    constructor(config: Config);
    /**
     * Configure and launch (if applicable) the object's environment.
     * @public
     * @return {q.promise} A promise which will resolve when the environment is
     *     ready to test.
     */
    protected setupDriverEnv(): q.Promise<any>;
}
