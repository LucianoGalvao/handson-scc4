/// <reference types="q" />
import * as q from 'protractor/built/driverProviders/node_modules/protractor/node_modules/@types/q';
import { Config } from 'protractor/built/driverProviders/node_modules/protractor/built/config';
import { DriverProvider } from 'protractor/built/driverProviders/node_modules/protractor/built/driverProviders/driverProvider';
export declare class Local extends DriverProvider {
    server_: any;
    constructor(config: Config);
    /**
     * Helper to locate the default jar path if none is provided by the user.
     * @private
     */
    addDefaultBinaryLocs_(): void;
    /**
     * Configure and launch (if applicable) the object's environment.
     * @public
     * @return {q.promise} A promise which will resolve when the environment is
     *     ready to test.
     */
    setupDriverEnv(): q.Promise<any>;
    /**
     * Teardown and destroy the environment and do any associated cleanup.
     * Shuts down the drivers and server.
     *
     * @public
     * @override
     * @return {q.promise} A promise which will resolve when the environment
     *     is down.
     */
    teardownEnv(): q.Promise<any>;
}
