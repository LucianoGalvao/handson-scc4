package br.com.scc4.handson.model;

import java.io.Serializable;
import java.text.ParseException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.swing.text.MaskFormatter;
import javax.validation.constraints.Email;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "destinatarios")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Destinatario implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String nomeCompleto;

    @Column(nullable =  false)
    private String telefone;

    @Column(nullable = false)
    private String cpf;

    @Column(nullable = false)
    private String endereco;

    @Email
    @Column(nullable = false)
    private String email;


    public Destinatario() {
    }

    public Destinatario(String nomeCompleto, String telefone, String cpf, String endereco, String email){
        super();
        this.nomeCompleto = nomeCompleto;
		this.telefone = telefone;
		this.cpf = cpf;
		this.endereco = endereco;
		this.email = email;
    }

    public static String formatString(String value, String pattern) {
        MaskFormatter mf;
        try {
            mf = new MaskFormatter(pattern);
            mf.setValueContainsLiteralCharacters(false);
            return mf.valueToString(value);
        } catch (ParseException ex){
            return value;
        }
    }

    public Long getId(){
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        if(telefone.length() == 10) {
            telefone = formatString(telefone, "(##) ####-####");
        } else if(telefone.length() == 11) {
            telefone = formatString(telefone, "(##) #####-####");
        }
        this.telefone = telefone;
    }

    public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = formatString(cpf, "###.###.###-##");;
	}
    public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
    }
    public Destinatario converter() {
		return new Destinatario(nomeCompleto, telefone, cpf, endereco, email);
	}
}