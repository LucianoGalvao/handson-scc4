package br.com.scc4.handson.controller;

import java.util.Map;
import java.util.List;
import java.util.HashMap;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.scc4.handson.exception.ResourceNotFoundException;
import br.com.scc4.handson.model.Remetente;
import br.com.scc4.handson.repository.RemetenteRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@CrossOrigin(origins = "*")
@RestController 
@RequestMapping("/api")
public class RemetenteController {
    @Autowired
    private RemetenteRepository remetenteRepository;

    @GetMapping("/remetentes")
    public List<Remetente> getAllRemetentes() {
        return remetenteRepository.findAll();
    }
    
    @GetMapping("/remetentes/{id}")
    public ResponseEntity<Remetente> getRemetenteById(@PathVariable(value = "id") Long remetenteId)
        throws ResourceNotFoundException {
            Remetente remetente = remetenteRepository.findById(remetenteId)
                .orElseThrow(() -> new ResourceNotFoundException("Remetente não encontrado com esse id :: " + remetenteId));
            return ResponseEntity.ok().body(remetente);
        }
    
    @PostMapping("/remetentes")
    public Remetente creatRemetente(@Valid @RequestBody Remetente remetente) {
        System.out.println(remetente);
        return remetenteRepository.save(remetente);
    }
    
    @PutMapping("/updateRemetente/{id}")
    public ResponseEntity<Remetente> updateRemetente(@PathVariable(value = "id")Long remetenteId, @Valid @RequestBody Remetente remetenteDetails)
        throws ResourceNotFoundException {
            Remetente remetente = remetenteRepository.findById(remetenteId)
            .orElseThrow(() -> new ResourceNotFoundException("Remetente não encontrado com esse id :: " + remetenteId));

            remetente.setNomeCompleto(remetenteDetails.getNomeCompleto());
            remetente.setEmail(remetenteDetails.getEmail());
            remetente.setCpf(remetenteDetails.getCpf());
            remetente.setEndereco(remetenteDetails.getEndereco());
            remetente.setEmail(remetenteDetails.getEmail());
            final Remetente updateRemetente = remetenteRepository.save(remetente);
            return ResponseEntity.ok(updateRemetente);
    }

    @DeleteMapping("/remetentes/{id}")
    public Map<String, Boolean> deleteRemetente(@PathVariable(value = "id") Long remetenteId)
        throws ResourceNotFoundException {
            Remetente remetente = remetenteRepository.findById(remetenteId)
            .orElseThrow(() -> new ResourceNotFoundException("Remetente não encontrado com esse id :: " + remetenteId));

            remetenteRepository.delete(remetente);
            Map<String, Boolean> response = new HashMap<>();
            response.put("Deletado", Boolean.TRUE);
            return response;        
        }
    
}
