package br.com.scc4.handson.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.scc4.handson.model.Remetente;


@Repository
public interface RemetenteRepository extends JpaRepository<Remetente, Long>{
    Remetente findById(long id);
    Boolean existsByEmail(String email);
}