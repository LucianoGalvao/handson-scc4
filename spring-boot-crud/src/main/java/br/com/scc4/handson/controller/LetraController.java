package br.com.scc4.handson.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class LetraController {
    
    @GetMapping("/tamanho")
    public String tamanhoPalavra(@RequestParam("palavra") String palavra) {
        return "tamanho="+palavra.length();
    }

    @GetMapping("/maisculas")
    public String palavraMaiscula(@RequestParam("palavra") String palavra) {
        return palavra.toUpperCase();
    }

    @GetMapping("/vogais")
    public String tiraConsoante(@RequestParam("palavra") String palavra) {
        return palavra.replaceAll("[^aeiouAEIOU]", "");
    }

    @GetMapping("/consoantes")
    public String tiraVogal(@RequestParam("palavra") String palavra){
        return palavra.replaceAll("[aeiouAEIOU]", "");
    }

    @GetMapping("/nomeBibliografico")
    public String nomeBibliografico(@RequestParam("nome") String nome) {
        String nomes[] = nome.split(" ");
        String nomeFinal = "";
        for(int i = 0; i < nomes.length;i++){
            if (i != nomes.length - 1) {
                String primeiraLetra = nomes[i].substring(0,1);
                String outrasLetras = nomes[i].substring(1);
                nomeFinal += primeiraLetra.toUpperCase()+outrasLetras+" ";
            } else {
                nomeFinal = nomes[i].toUpperCase() + ", " + nomeFinal;
            }
        }
        return nomeFinal;
    }
}
    