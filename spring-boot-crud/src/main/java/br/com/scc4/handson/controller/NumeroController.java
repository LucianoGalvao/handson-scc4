package br.com.scc4.handson.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class NumeroController {

    @GetMapping("/listaReversa")
    public List<Integer> listaReversa(@RequestParam("lista") List<Integer> lista) {
        Collections.reverse(lista);
        return lista;
    }

    @GetMapping("/imprimirImpares")
    public List<Integer> imprimeImpares(@RequestParam("lista") List<Integer> lista) {
        List<Integer> retorno = new ArrayList<>();
        for (Integer numero : lista) {
            if (numero % 2 != 0)
            retorno.add(numero);
        }
        return retorno;
    }
    
    @GetMapping("/imprimirPares")
    public List<Integer> imprimePares(@RequestParam("lista") List<Integer> lista) {
        List<Integer> retorno = new ArrayList<>();
        for (Integer numero : lista) {
            if (numero % 2 == 0)
            retorno.add(numero);
        }
        return retorno;
    }
    
} 