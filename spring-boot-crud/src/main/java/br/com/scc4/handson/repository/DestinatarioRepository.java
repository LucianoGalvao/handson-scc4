package br.com.scc4.handson.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.scc4.handson.model.Destinatario;


public interface DestinatarioRepository extends JpaRepository<Destinatario, Long>{
    
}