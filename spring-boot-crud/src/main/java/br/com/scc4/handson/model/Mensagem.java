package br.com.scc4.handson.model;

import java.util.List;

public class Mensagem {
    private String nomeRemetente;
    private List<String> listaDestinatarioMensagem;
    private String mensagemForm;

    public String getNomeRemetente(){
        return nomeRemetente;
    }
    public void setNomeRemetente(String nomeRemetente) {
        this.nomeRemetente = nomeRemetente;
    }
    public List<String> getListaDestinatarioMensagem() {
        return listaDestinatarioMensagem;
    }
    public void setListaDestinatarioMensagem(List<String> listaDestinatarioMensagem) {
        this.listaDestinatarioMensagem = listaDestinatarioMensagem;
    }
    public String getMensagemForm(){
        return mensagemForm;
    }
    public void setMensagemForm(String mensagemForm){
        this.mensagemForm = mensagemForm;
    }
}