package br.com.scc4.handson.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.scc4.handson.model.Fracao;
import br.com.scc4.handson.model.SistemaMonetario;

@RestController
@RequestMapping("/parte-1")
public class ParteUmController {
	
	
	@GetMapping("/sistemaMonetario")
	public String SistemaMonetario(@RequestParam("saque") int saque) {
		SistemaMonetario sistemaMonetario = new SistemaMonetario();
		sistemaMonetario.setSaque(saque);
		return sistemaMonetario.getSaque();
	}
	
	@GetMapping("/calculadoraFracao")
	public String CaculadoraFracao() {
		Fracao calc = new Fracao();
		Fracao f1 = new Fracao(1, 4);
        Fracao f2 = new Fracao(4, 5);
        calc.soma(f1, f2);
		return "Simplificada: " + calc.toString() + "<br>" + " Fração: " + calc.getResultadoSimplificado();
	}

}
