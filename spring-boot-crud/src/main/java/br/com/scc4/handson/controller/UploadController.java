package br.com.scc4.handson.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import br.com.scc4.handson.controller.form.DestinatarioForm;
import br.com.scc4.handson.model.Destinatario;
import br.com.scc4.handson.repository.DestinatarioRepository;


@RestController
@RequestMapping("/destinatarios")
public class UploadController {

	@Autowired
    private DestinatarioRepository destinatarioRepository;

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/upload-csv-destinatarios")
    public void uploadCSVFile(@RequestParam("avatar")
    MultipartFile file, Model model) throws IOException {
        if(file.isEmpty()) {
            model.addAttribute("mensagem", "Por favor, selecione um arquivo CSV para upload.");
            model.addAttribute("status", false);
        } else {
            try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
                CsvToBean<DestinatarioForm> csvToBean = new CsvToBeanBuilder<DestinatarioForm>(reader)
                    .withType(DestinatarioForm.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

                List<DestinatarioForm> destinatarios = csvToBean.parse();

                for (DestinatarioForm destinatarioForm : destinatarios) {
                    Destinatario destinatario = new Destinatario();
                    destinatario.setNomeCompleto(destinatarioForm.getNomeCompleto());
                    destinatario.setTelefone(destinatarioForm.getTelefone());
                    destinatario.setCpf(destinatarioForm.getCpf());
                    destinatario.setEndereco(destinatarioForm.getEndereco());
					destinatario.setEmail(destinatarioForm.getEmail());

                    destinatarioRepository.save(destinatario);
                }

                model.addAttribute("destinatarios", destinatarios);
                model.addAttribute("status", true);
            } catch (Exception ex) {
                model.addAttribute("message", "Um erro foi encontrado no processamento do arquivo CSV.");
                model.addAttribute("status", false);
            }
        }
    }
}