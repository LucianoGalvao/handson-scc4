package br.com.scc4.handson.model;

public class Fracao {
	
	private int numerador;
	private int denominador;
	private float resultado;
	private float resultadoSimplificado;
	
	public Fracao() {
	}
	
	public Fracao(int num, int den) {
		if (den == 0)
            throw new IllegalArgumentException("Denominador não pode ser igual a zero.");
		this.numerador = num;
		this.denominador = den;
	}

	public Fracao soma(Fracao f1, Fracao f2) {
		numerador = (f1.numerador * f2.denominador) + (f2.numerador * f1.denominador);
		denominador = f1.denominador * f2.denominador;
		resultado = numerador / denominador;
		
		resultadoSimplificado = ((float)f1.numerador / (float)f1.denominador) + ((float)f2.numerador / (float)f2.denominador);
		System.out.println(resultadoSimplificado);
		
		 
		return new Fracao(this.numerador, this.denominador);
	}
	

	public Fracao subtracao(Fracao f1, Fracao f2) {
		numerador = (f1.numerador * f2.denominador) - (f2.numerador * f1.denominador);
		denominador = f1.denominador * f2.denominador;
		resultado = numerador / denominador;
		
		 
		return new Fracao(numerador, denominador);
	}
	
	public Fracao multiplicacao(Fracao f1, Fracao f2) {
		numerador = f1.numerador * f2.numerador;
		denominador = f2.denominador * f1.denominador;
		resultado = numerador / denominador;
		return new Fracao(numerador, denominador);
	}
	
	public Fracao divisao(Fracao f1, Fracao f2) {
		numerador = f1.numerador * f2.denominador;
		denominador = f1.denominador * f2.numerador;
		resultado = numerador / denominador;
		return new Fracao(numerador, denominador);
	}
	

	public String toString(){
		return String.format("%d/%d\t", numerador, denominador);
	}

	public float getResultado() {
		return resultado;
	}

	public void setResultado(float resultado) {
		this.resultado = resultado;
	}

	public float getResultadoSimplificado() {
		return resultadoSimplificado;
	}

	public void setResultadoSimplificado(float resultadoSimplificado) {
		this.resultadoSimplificado = resultadoSimplificado;
	}
	

}
