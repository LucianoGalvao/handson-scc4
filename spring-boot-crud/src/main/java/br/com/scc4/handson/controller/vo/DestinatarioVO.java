package br.com.scc4.handson.controller.vo;

import java.util.List;
import java.util.stream.Collectors;

import br.com.scc4.handson.model.Destinatario;

public class DestinatarioVO {
    private Long id;
    private String nomeCompleto;
    private String telefone;
    private String cpf;
    private String endereco;
    private String email;

    public DestinatarioVO(Destinatario destinatario) {
        this.id = destinatario.getId();
        this.nomeCompleto = destinatario.getNomeCompleto();
        this.telefone = destinatario.getTelefone();
        this.endereco = destinatario.getEndereco();
        this.email = destinatario.getEmail();
    }

    public Long getId() {
        return id;
    }

    public String getNomeCompleto(){
        return nomeCompleto;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getCpf() {
        return cpf;
    }

    public String getEndereco() {
        return endereco;
    }

    public String getEmail() {
        return email;
    }

    public static List<DestinatarioVO> converter(List<Destinatario> listaDestinatario) {
        return listaDestinatario.stream().map(DestinatarioVO::new).collect(Collectors.toList());
    }
}