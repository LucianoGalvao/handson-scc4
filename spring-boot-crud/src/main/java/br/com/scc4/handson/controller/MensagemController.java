package br.com.scc4.handson.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.scc4.handson.controller.vo.DestinatarioVO;
import br.com.scc4.handson.model.Destinatario;
import br.com.scc4.handson.model.Mensagem;
import br.com.scc4.handson.model.Remetente;
import br.com.scc4.handson.repository.DestinatarioRepository;

@RestController
@RequestMapping("/destinatarios")
public class MensagemController {
    @Autowired
    private DestinatarioRepository destinatarioRepository;
    
    public void enviaMensagem(Remetente remetente, List<Destinatario> destinatarios) {

    }
    
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/listarDestinatararios")
    public List<DestinatarioVO> listaDestinatarios() {
        List<Destinatario> destinararios = destinatarioRepository.findAll();
        return DestinatarioVO.converter(destinararios);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/recebeMensagem")
    @Transactional
    public void mensagem(@RequestBody Mensagem mensagem) {
        for (String destinatario : mensagem.getListaDestinatarioMensagem()) {
            System.out.println(mensagem.getNomeRemetente() + " enviou mensagem para " + destinatario + ": " + mensagem.getMensagemForm());
        }
    }

}