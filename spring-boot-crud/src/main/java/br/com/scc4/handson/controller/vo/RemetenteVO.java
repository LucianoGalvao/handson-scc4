package br.com.scc4.handson.controller.vo;

import java.util.List;
import java.util.stream.Collectors;

import br.com.scc4.handson.model.Remetente;

public class RemetenteVO {
    private Long id;
    private String nomeCompleto;
    private String telefone;
    private String cpf;
    private String endereco;
    private String email;

    public RemetenteVO(Remetente remetente) {
        this.id = remetente.getId();
        this.nomeCompleto = remetente.getNomeCompleto();
        this.telefone = remetente.getTelefone();
        this.endereco = remetente.getEndereco();
        this.email = remetente.getEmail();
    }

    public Long getId() {
        return id;
    }

    public String getNomeCompleto(){
        return nomeCompleto;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getCpf() {
        return cpf;
    }

    public String getEndereco() {
        return endereco;
    }

    public String getEmail() {
        return email;
    }

    public static List<RemetenteVO> converter(List<Remetente> listaRemetente) {
        return listaRemetente.stream().map(RemetenteVO::new).collect(Collectors.toList());
    }
}