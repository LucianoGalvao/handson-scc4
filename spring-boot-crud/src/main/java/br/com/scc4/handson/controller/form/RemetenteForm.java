package br.com.scc4.handson.controller.form;

import com.opencsv.bean.CsvBindByName;

public class RemetenteForm {
    
    @CsvBindByName
    private String nomeCompleto;

    @CsvBindByName
    private String telefone;

    @CsvBindByName
    private String cpf;

    @CsvBindByName
    private String endereco;

    @CsvBindByName
    private String email;

    public RemetenteForm(String nomeCompleto, String telefone, String cpf, String endereco, String email) {
        super();
        this.nomeCompleto = nomeCompleto;
        this.telefone = telefone;
		this.cpf = cpf;
        this.endereco = endereco;
        this.email = email;
    }

    public String getNome() {
        return nomeCompleto;
    }

    public void setNome(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }
    public String getTelefone() {
        return telefone;
    }

    public void getTelefone(String telefone) {
        this.telefone = telefone;
    }
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}